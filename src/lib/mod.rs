use num_bigint::BigUint;
use rand::Rng;
use sprimes_disp::{SafePrimeError, SafePrimesSource};
use std::fmt::Display;
use std::{error, fmt, result};

pub type Result<T> = result::Result<T, GeneratorError>;

/// ## Errors
///
#[derive(Debug)]
pub struct GeneratorError {
    e: Option<Box<dyn error::Error>>,
    message: String,
}
impl Display for GeneratorError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Generator error cause by {:?}", self.e)
    }
}

impl error::Error for GeneratorError {}

impl From<Box<dyn error::Error>> for GeneratorError {
    fn from(err: Box<dyn error::Error>) -> Self {
        Self {
            e: Some(err),
            message: "".to_string(),
        }
    }
}

impl From<&String> for GeneratorError {
    fn from(msg: &String) -> Self {
        Self {
            e: None,
            message: msg.to_owned(),
        }
    }
}
impl From<String> for GeneratorError {
    fn from(msg: String) -> Self {
        Self {
            e: None,
            message: msg.to_owned(),
        }
    }
}

impl From<SafePrimeError> for GeneratorError {
    fn from(e: SafePrimeError) -> Self {
        let mut s = String::from(format!("Error from SafePrimeSource {} ", e));

        Self {
            e: Some(Box::new(e)),
            message: s.to_owned(),
        }
    }
}
pub struct Generator {
    p_mp: BigUint,
    g_mp: BigUint,
    e2: u64,
    s1: u64,
    s2: u64,
    a: u64,
    p: u64,
    psource: SafePrimesSource,
}

impl Generator {
    fn try_from(filename: &String) -> Result<Self> {
        let psource = SafePrimesSource::try_from(filename)?;

        let def_prime = vec![
            1, 1, 5, 7, 9, 2, 0, 8, 9, 2, 3, 7, 3, 1, 6, 1, 9, 5, 4, 2, 3, 5, 7, 0, 9, 8, 5, 0, 0,
            8, 6, 8, 7, 9, 0, 7, 8, 5, 3, 2, 6, 9, 9, 8, 4, 6, 6, 5, 6, 4, 0, 5, 6, 4, 0, 3, 9, 4,
            5, 7, 5, 8, 4, 0, 0, 7, 9, 1, 3, 1, 2, 9, 8, 7, 0, 1, 2, 7,
        ];
        let z = rand::thread_rng().gen_range(2..u64::MAX);
        let s1 = rand::thread_rng().gen_range(2..u64::MAX);
        let s2 = rand::thread_rng().gen_range(2..u64::MAX);
        let p_mp = BigUint::new(def_prime);

        Ok(Generator {
            g_mp: (&p_mp - z * z) % &p_mp,
            p_mp,
            e2: 9,
            s1,
            s2,
            a: 2731,
            p: 1152921504606849707,
            psource,
        })
    }
}
