# Feistel-Exp CSPRG
This is an implementation in pure Rust of the CSPRG given in the paper [Design and implementation of a novel cryptographically secure pseudorandom number generator](https://link.springer.com/article/10.1007/s13389-022-00297-8)
